var fs = require("fs");
var targetList = require("/app/targets.json");

let targets = {
	get: (resolve, reject) => {
//		console.log(targetList);
		resolve(targetList);
	},
	post: (req, resolve, reject) => {
//		console.log(targetList);
//		console.log(req);
//		console.log(JSON.stringify(req.body));
		targetList.push(req.body);
//		console.log(targetList);
		resolve(targetList);
	}
};

module.exports = targets;
