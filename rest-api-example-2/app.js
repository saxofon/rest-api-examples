var express = require("express");
var bodyparser = require("body-parser");
var router = express.Router();
var app = express();

const PORT = 80;

let targets = require("/app/targets.js");

app.use("/", router);

app.use(bodyparser.urlencoded({extended:false}));
app.use(bodyparser.json());

router.get("/targetList", (req, res, next) => {
	targets.get((data) => {
		res.send(data);
	}, (error) => {
		next(error);
	});
});

router.get("/target/:name", (req, res, next) => {
	targets.get((data) => {
		const target = data.find((item) => item.name === req.params.name);
		if (!target) {
			res.status(500).send("Target not found");
		} else {
			res.send(target);
		}
	}, (error) => {
		next(error);
	});
});

router.post("/target", (req, res) => {
	console.log(req.body);
	targets.post(req, (data) => {
		res.send(data);
	}, (error) => {
		next(error);
	});
});

router.put("/targetList", (req, res, next) => {
	targets.put(req, (data) => {
		res.send(data);
	}, (error) => {
		next(error);
	});
});

app.use("/api/v1", router);
app.use(express.json());

app.listen(PORT, () => {
	console.log(`Server running on port ${PORT}`);
});

