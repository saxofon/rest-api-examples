#/bin/bash

PORT=$1

echo -e "\n$0: content before"
curl http://localhost:${PORT}/api/v1/targetList

echo -e "\n$0: add new item"

curl --header 'Content-Type: application/json' --request POST \
	--data '{"name":"dus6630r","console":"arn-digi1.wrs.com:2013","IPaddress":"128.224.95.188"}' \
	http://localhost:${PORT}/api/v1/target

echo -e "\n$0: content after"
curl http://localhost:${PORT}/api/v1/targetList

echo ""
