# Collection of REST API examples

## Usage

### Build
make images

### Run
make test-containers

make test-api

### Cleanup
make clean

## Best practices links
https://www.vinaysahni.com/best-practices-for-a-pragmatic-restful-api
https://stackoverflow.blog/2020/03/02/best-practices-for-rest-api-design

## Inspiration links

### Examples Daniel Gold - Build your own REST API from scrach
http://www.danielgold.net/2020/07/build-your-own-rest-api-from-scratch.html
http://www.danielgold.net/2020/08/build-your-own-rest-api-from-scratch.html

It's a simple get data part.

## Example 2 extends example 1 with post data

