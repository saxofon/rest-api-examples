TOP := $(shell pwd)

PORTBASE = 9080

#APPS += rest-api-example-1
APPS += rest-api-example-2

images:  $(APPS)
	$(foreach APP,$(APPS),podman build -t $(APP) $(TOP)/$(APP);)

test-containers:
	$(foreach APP,$(APPS),podman run --rm --rmi -d -it -p $$((index++ + $(PORTBASE))):80 --name $(APP) $(APP);)

test-api:
	$(foreach APP,$(APPS),sh $(APP)/test-api.sh $$((index++ + $(PORTBASE)));echo "";)

clean:
	podman kill $(APPS)
