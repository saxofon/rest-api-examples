var express = require("express");
var app = express();
var router = express.Router();
const PORT = 80;

let targets = require("/app/targets.js");

router.get("/targetList", (req, res, next) => {
	targets.get((data) => {
		res.status(200).json({
			"status": 200,
			"statusText": "OK",
			"data": data
		});
	}, (error) => {
		next(error);
	});
});

app.use("/api/v1", router);

app.listen(PORT, () => {
	console.log(`Server running on port ${PORT}`);
});

