var fs = require("fs");
const f = "/app/targets.json";

let targets = {
	get: (resolve, reject) => {
		fs.readFile(f, (error, data) => {
			if (error) {
				reject(error);
			} else {
				resolve(JSON.parse(data));
			}
		});
	}
};

module.exports = targets;
